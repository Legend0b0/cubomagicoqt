#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGuiApplication>
#include <QScreen>

class AboutWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit AboutWindow(QWidget *parent = nullptr);
    ~AboutWindow();

protected:
    void configureWindow();
    void widgets();
    void layouts();

private:
    QLabel *icon = nullptr;
    QLabel *name = nullptr;
    QVector<QLabel*> *credits = nullptr;

    QWidget *layoutVWindow = nullptr;
    QWidget *layoutHWindow1 = nullptr;
    QWidget *layoutHWindow2 = nullptr;
    QWidget *layoutHWindow3 = nullptr;
    QWidget *layoutFWindow = nullptr;

    QVBoxLayout *mainLayout = nullptr;
    QHBoxLayout *hLayout1 = nullptr;
    QHBoxLayout *hLayout2 = nullptr;
    QHBoxLayout *hLayout3 = nullptr;
    QFormLayout *fLayout = nullptr;

public slots:
    void lightTheme();
    void darkTheme();
};

#endif // ABOUTWINDOW_H
