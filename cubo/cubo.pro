QT += core widgets gui openglwidgets opengl

CONFIG += c++17 cmdline

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        aboutwindow.C \
        glwidget.C \
        glwidgetmenu.C \
        helpwindow.C \
        main.C \
        trackball.C \
        window.C

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    aboutwindow.h \
    glwidget.h \
    glwidgetmenu.h \
    helpwindow.h \
    trackball.h \
    window.h

LIBS += -lGLU -lglut

RESOURCES += \
    Resource_File.qrc

FORMS +=
