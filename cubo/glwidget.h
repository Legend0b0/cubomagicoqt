#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QWidget>
#include <QPushButton>
#include <QOpenGLWidget>
#include <QSurfaceFormat>
#include <QOpenGLFunctions>
#include <QtOpenGL>
#include <GL/glu.h>
#include <QTimer>
#include <QEventLoop>
#include <QDebug>
#include <QLabel>
#include "trackball.h"

class Face_Part
{
public:
    bool colored;
    char axis;
    float color[3];
};

class Part
{
public:
    int index = 0;
    float position[2][4][3];
    Face_Part faces[6];
};

class Cube
{
public:
    Part part[3][3][3];
};

class Node
{
public:
    Part *self = nullptr;
    Node *next = nullptr;
};

class Side
{
public:
    Node *head = nullptr;

    Part *center = nullptr;
    Part *up = nullptr;
    Part *left = nullptr;
    Part *right = nullptr;
    Part *down = nullptr;
    Part *up_left = nullptr;
    Part *up_right = nullptr;
    Part *down_left = nullptr;
    Part *down_right = nullptr;
};

class Grapho
{
public:
    Node *head_x = nullptr;
    Node *head_y = nullptr;
    Node *head_z = nullptr;

    Side *blue = new Side;
    Side *green = new Side;
    Side *red = new Side;
    Side *orange = new Side;
    Side *yellow = new Side;
    Side *white = new Side;
};

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    GLWidget(QVector<QLabel*> *parent_labels = nullptr, QVector<QPushButton*> *parent_buttons = nullptr, QTimer *parent_timer = nullptr, QWidget *parent = nullptr);
    ~GLWidget();

    bool move = false;
    bool reseted = false;
    bool shuffled = false;
    Grapho *grapho = new Grapho;

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void createCuboMagico();

    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void getCameraPosition();

    void orientationBlue();
    void orientationGreen();
    void orientationRed();
    void orientationOrange();
    void orientationYellow();
    void orientationWhite();

    Node *createNode(Part *self);
    void createList(Side *side);
    void createMidList();
    void destroyNode(Node *node);
    void rotatePointersClockwise(Side *side);
    void rotatePointersAntiClockwise(Side *side);
    void refreshFacePointers(Side *side);
    void refreshAllPointers();
    void printPositions();

    void rotateX(Node *head, int angle);
    void rotateY(Node *head, int angle);
    void rotateZ(Node *head, int angle);

    void rotateFaceAxysX(Node *head);
    void rotateFaceAxysY(Node *head);
    void rotateFaceAxysZ(Node *head);

private:
    bool m_core;
    int viewport[4];
    int Orientation = -1;
    int Orientation_Index[4][16];
    double matModelView[16];
    double matProjection[16];
    Cube cube;
    TrackBall trackBall;
    GLdouble camera_pos[3];
    QTimer *timer = nullptr;
    QVector<QLabel*> *labels = nullptr;
    QVector<QPushButton*> *buttons = nullptr;

public slots:
    void rotateBlue();
    void rotateBlueL();
    void rotateGreen();
    void rotateGreenL();
    void rotateRed();
    void rotateRedL();
    void rotateOrange();
    void rotateOrangeL();
    void rotateYellow();
    void rotateYellowL();
    void rotateWhite();
    void rotateWhiteL();
    void rotateMidX();
    void rotateMidXL();
    void rotateMidY();
    void rotateMidYL();
    void rotateMidZ();
    void rotateMidZL();

    void shuffle();
    void reset();
    void trade();
    void catchVerify();

signals:
    void throwSolved();
    void throwVerify();
    void throwMenu();
    void throwStart();
};

#endif // GLWIDGET_H
