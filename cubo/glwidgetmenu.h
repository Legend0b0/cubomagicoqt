#ifndef GLWIDGETMENU_H
#define GLWIDGETMENU_H

#include <QOpenGLWidget>
#include <QSurfaceFormat>
#include <QOpenGLFunctions>
#include <QtOpenGL>
#include <GL/glu.h>
#include <QPushButton>
#include "glwidget.h"

class GLWidgetMenu : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    GLWidgetMenu(QWidget *parent = nullptr);

    Grapho *grapho = nullptr;

protected:
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

private:
    bool m_core;

public slots:
    void createDesenho();
    void att();
};

#endif // GLWIDGETMENU_H
