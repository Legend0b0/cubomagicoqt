#ifndef HELPWINDOW_H
#define HELPWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFormLayout>
#include <QGuiApplication>
#include <QScreen>

class HelpWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit HelpWindow(QWidget *parent = nullptr);
    ~HelpWindow();

protected:
    void configureWindow();
    void widgets();
    void layouts();

private:
    QVector<QLabel*> *commands = nullptr;
    QVector<QLabel*> *faceMove = nullptr;
    QVector<QLabel*> *faceShortcut = nullptr;
    QVector<QLabel*> *menuOption = nullptr;
    QVector<QLabel*> *menuShortcut = nullptr;

    QWidget *layoutVWindow = nullptr;
    QWidget *layoutFWindow1 = nullptr;
    QWidget *layoutFWindow2 = nullptr;
    QWidget *layoutHWindow1 = nullptr;
    QWidget *layoutHWindow2 = nullptr;
    QWidget *layoutHWindow3 = nullptr;

    QVBoxLayout *mainLayout = nullptr;
    QFormLayout *fLayout1 = nullptr;
    QFormLayout *fLayout2 = nullptr;
    QHBoxLayout *hLayout1 = nullptr;
    QHBoxLayout *hLayout2 = nullptr;
    QHBoxLayout *hLayout3 = nullptr;

public slots:
    void lightTheme();
    void darkTheme();
};

#endif // HELPWINDOW_H
