#ifndef TRACKBALL_H
#define TRACKBALL_H

#include <QVector3D>
#include <QMatrix4x4>
#include <QQuaternion>
#include <QTime>

class TrackBall
{
public :
    void setSize(int w, int h);
    float *get(void);
    void setCur(int x, int y);
    QVector3D mapSphere(QVector3D mouse);
    void toMatrix(QMatrix4x4 & m, QQuaternion & q);
    void updatePosition(void);
    void mousePress(void);
    void mouseRelease(void);

private :
    bool drag = false;
    int width;
    int height;
    QVector3D v_cur = QVector3D(0.0, 0.0, 0.0);
    QVector3D v_down = QVector3D(0.0, 0.0, 0.0);
    QQuaternion q_cur = QQuaternion(1.0, 0.0, 0.0, 0.0);
    QQuaternion q_end = QQuaternion(1.0, 0.0, 0.0, 0.0);
    QMatrix4x4 mat_cur;
};

# endif
