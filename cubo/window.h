#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGridLayout>
#include <QDebug>
#include <QMenuBar>
#include <QtWidgets>
#include <QAction>
#include <QTimer>
#include "glwidget.h"
#include "glwidgetmenu.h"
#include "aboutwindow.h"
#include "helpwindow.h"

class Window : public QMainWindow
{
    Q_OBJECT
public:
    explicit Window(QWidget *parent = nullptr);
    ~Window();

    QMenu *toolsMenu = nullptr;
    QMenu *themeMenu = nullptr;
    QMenu *helpMenu = nullptr;
    QAction *shuffleAct = nullptr;
    QAction *resetAct = nullptr;
    QAction *tradeAct = nullptr;
    QAction *exitAct = nullptr;
    QAction *darkThemeAct = nullptr;
    QAction *lightThemeAct = nullptr;
    QAction *helpAct = nullptr;
    QAction *aboutAct = nullptr;
    QVector<QLabel*> *labels = nullptr;
    QVector<QPushButton*> *buttons = nullptr;
    QPushButton *resetTButton = nullptr;
    QPushButton *resetRButton = nullptr;

private:
    int time[3];
    int timerRecord[3];
    GLWidget *cube = nullptr;
    GLWidgetMenu *glmenu = nullptr;
    QTimer *timer = nullptr;
    HelpWindow *help = nullptr;
    AboutWindow *about = nullptr;

    QWidget *mainLayoutWindow = nullptr;
    QWidget *cuboLayoutWindow = nullptr;
    QWidget *upLeftLayoutWindow = nullptr;
    QWidget *topLayoutWindow = nullptr;
    QWidget *upRightLayoutWindow = nullptr;
    QWidget *leftLayoutWindow = nullptr;
    QWidget *rightLayoutWindow = nullptr;
    QWidget *downLeftLayoutWindow = nullptr;
    QWidget *botLayoutWindow = nullptr;
    QWidget *downRightLayoutWindow = nullptr;
    QWidget *menuLayoutWindow = nullptr;
    QWidget *menuInHLayoutWindow1 = nullptr;
    QWidget *menuInHLayoutWindow2 = nullptr;
    QWidget *menuInHLayoutWindow3 = nullptr;

    QHBoxLayout *mainLayout = nullptr;
    QGridLayout *cuboLayout = nullptr;
    QHBoxLayout *upLeftLayout = nullptr;
    QHBoxLayout *topLayout = nullptr;
    QHBoxLayout *upRightLayout = nullptr;
    QVBoxLayout *leftLayout = nullptr;
    QVBoxLayout *rightLayout = nullptr;
    QHBoxLayout *downLeftLayout = nullptr;
    QHBoxLayout *botLayout = nullptr;
    QHBoxLayout *downRightLayout = nullptr;
    QVBoxLayout *menuLayout = nullptr;
    QHBoxLayout *menuInHLayout1 = nullptr;
    QHBoxLayout *menuInHLayout2 = nullptr;
    QHBoxLayout *menuInHLayout3 = nullptr;

    QScrollArea *cuboScrollArea = nullptr;
    QScrollArea *menuScrollArea = nullptr;

protected:
    void configureWindow();
    void resizeEvent(QResizeEvent *event);
    void createWidgets();
    void createMenuBar();
    void createLayouts();
    void settingLayouts();
    void connects();

public slots:
    void helpWindow();
    void aboutWindow();
    void lightTheme();
    void darkTheme();

    void startTimer();
    void stopTimer();
    void resetTimer();
    void resetRecord();
    void stopWatch();

signals:
    void throwLightTheme();
    void throwDarkTheme();
};

#endif // WINDOW_H
